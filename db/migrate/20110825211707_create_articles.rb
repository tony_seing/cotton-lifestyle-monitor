class CreateArticles < ActiveRecord::Migration
  def self.up
    create_table :articles do |t|
      t.date :creation_date
      t.string :title
      t.string :subtitle
      t.text :summary
      t.text :image_summary

      t.timestamps
    end
  end

  def self.down
    drop_table :articles
  end
end
