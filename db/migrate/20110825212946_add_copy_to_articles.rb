class AddCopyToArticles < ActiveRecord::Migration
  def self.up
    add_column :articles, :copy, :text
  end

  def self.down
    remove_column :articles, :copy, :text
  end
end
