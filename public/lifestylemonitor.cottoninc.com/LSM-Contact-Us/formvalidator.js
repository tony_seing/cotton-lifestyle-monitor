function checkFields() {
missinginfo = "";
if (document.form.Name.value == "") {
missinginfo += "\n     -  Name";
}

if ((document.form.Email.value == "") || 
(document.form.Email.value.indexOf("@") == -1) || 
(document.form.Email.value.indexOf(".") == -1)) {
missinginfo += "\n     -  Email";
}

if (document.form.Comment.value == "") {
missinginfo += "\n     -  Comment";
}

if (missinginfo != "") {
missinginfo ="_____________________________\n" +
"Please include following field(s):\n" +
missinginfo + "\n_____________________________" +
"\nPlease re-enter and submit again!";
alert(missinginfo);
return false;
}
else return true;
}
