<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
	<title>Cotton Incorporated - Market Monthly Economic Letter</title>
	<link>http://www.cottoninc.com/MarketInformation/MonthlyEconomicLetter/</link>
	<description>This page contains the most recent Monthly U.S. Cotton Market Economic Letter for 2011.</description>
	<copyright>2011 Cotton Incorporated. All rights reserved; America's Cotton Producers and Importers.</copyright>
	<docs>http://www.rssboard.org/rss-specification/</docs>
	<language>en-us</language>
	<managingEditor>JDevine@cottoninc.com (Jon Devine)</managingEditor>
	<webMaster>webmaster@cottoninc.com (Webmaster)</webMaster>

	<atom:link href="http://www.cottoninc.com/MarketInformation/MonthlyEconomicLetter/RSS.cfm" rel="self" type="application/rss+xml" />

	<image>
		<url>http://resource.cottoninc.com/_images/cottoninc_header/cottonbrown.jpg</url>
		<title>Cotton Incorporated - Market Monthly Economic Letter</title>
		<link>http://www.cottoninc.com/MarketInformation/MonthlyEconomicLetter/</link>
		<width>85</width>
		<height>55</height>
	</image>

	<item>
		<title>U.S. Cotton Market Monthly Economic Letter - August 12,  2011</title>
		<link>http://www.cottoninc.com/MarketInformation/MonthlyEconomicLetter/</link>
		<description>This page contains the most recent monthly U.S. Cotton Market Economic Letter for 2011, dated August 12, 2011.</description>
		<pubDate>Fri, 12 Aug 2011 00:00:00 EST</pubDate>
		<guid isPermaLink="false">{ts '2011-08-12 00:00:00'}</guid>
		<author>JDevine@cottoninc.com (Jon Devine)</author>
	</item>
</channel>
</rss>
