<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
<channel>
	<title>Cotton Incorporated - Crop Quality Summary Weekly</title>
	<link>http://www.cottoninc.com/CropQualitySummary/</link>
	<description>This page contains the most recent Weekly Quality Summary for 2011 U.S. Upland Crop. This report was produced by the staff of Cotton Incorporated's Fiber Quality Research Division and made available to the cotton community.</description>
	<copyright>© 2011 Cotton Incorporated. All rights reserved; America's Cotton Producers and Importers.</copyright>
	<docs>http://www.rssboard.org/rss-specification/</docs>
	<language>en-us</language>
	<managingEditor>MWatson@cottoninc.com (Mike Watson)</managingEditor>
	<webMaster>webmaster@cottoninc.com (Webmaster)</webMaster>

	<image>
		<url>http://resource.cottoninc.com/_images/cottoninc_header/cottonbrown.jpg</url>
		<title>Cotton Incorporated</title>
		<link>http://www.cottoninc.com</link>
		<width>85</width>
		<height>55</height>
	</image>

	<item>
		<title>Crop Quality Summary Week Ending  - August 11,  2011</title>
		<link>http://www.cottoninc.com/CropQualitySummary/CropQualitySummaryWeekly/</link>
		<description>This page contains the most recent Weekly Quality Summary for 2011 U.S. Upland Crop. This report was produced by the staff of Cotton Incorporated's Fiber Quality Research Division and made available to the cotton community.</description>
		<pubDate>Tue, 23 Aug 2011 15:24:16 EST</pubDate>
		<guid isPermaLink="false">2011-08-23 15:24:16.36</guid>
		<author>MWatson@cottoninc.com (Mike Watson)</author>
	</item>
</channel>
</rss>
