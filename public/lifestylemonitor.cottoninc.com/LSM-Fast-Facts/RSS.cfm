<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
	<title>Cotton Incorporated - Lifestyle Monitor Fast Facts</title>
	<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/</link>
	<description>Cotton Incorporated Fast Facts features select data and analyses from the company's Lifestyle Monitor consumer surveys, as well as its Retail Monitor and Supply Chain Insights industry reports.</description>
	<language>en-us</language>
	<copyright>2011 Cotton Incorporated. All rights reserved; America's Cotton Producers and Importers.</copyright>
	<docs>http://www.rssboard.org/rss-specification/</docs>
	<managingEditor>EThompson@cottoninc.com (Emily Thompson)</managingEditor>
	<webMaster>webmaster@cottoninc.com (Webmaster)</webMaster>
	<generator>Adobe ColdFusion 8</generator>

	<atom:link href="http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/RSS.cfm" rel="self" type="application/rss+xml" />

	<image>
		<url>http://resource.cottoninc.com/_images/cottoninc_header/cottonbrown.jpg</url>
		<title>Cotton Incorporated - Lifestyle Monitor Fast Facts</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/</link>
		<width>85</width>
		<height>55</height>
	</image>

	<item>
		<title>Cotton Prices:  What’s Next? Podcast</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/Cotton-Prices-Whats-Next-Podcast/</link>
		<description>Learn How Demand Impacts Cotton Prices and What to Expect Next</description>
		<pubDate>Wed, 25 May 2011 14:52:52 EST</pubDate>
		<guid isPermaLink="false">2011-05-25 14:52:52.783</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>What sources are most likely to influence women to purchase new apparel?</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/053-Sources-Influence-Women-Purchase-New-Apparel/</link>
		<description>What sources are most likely to influence women to purchase new apparel?</description>
		<pubDate>Fri, 06 May 2011 12:07:05 EST</pubDate>
		<guid isPermaLink="false">2011-05-06 12:07:05.78</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>How much time do consumers spend browsing the internet for clothing each month?</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/029-Consumers-Time-Browsing-Internet-for-Clothing/</link>
		<description>On average, consumers spend over an hour and a half each month browsing the internet for clothing.  </description>
		<pubDate>Thu, 05 May 2011 12:06:01 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 12:06:01.733</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>How many pairs of denim jeans do consumers own?</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/001-How-many-pairs-denim-jeans-do-consumers-own/</link>
		<description>On average, consumers own about 7 pairs of denim jeans. </description>
		<pubDate>Thu, 05 May 2011 11:57:34 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:57:34.733</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>How many pairs of denim jeans do men own?</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/002-How-many-pairs-denim-jeans-do-men-own/</link>
		<description>On average, men own about 6 pairs of denim jeans. </description>
		<pubDate>Thu, 05 May 2011 11:57:24 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:57:24.64</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>How many pairs of denim jeans do women own?</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/003-How-many-pairs-denim-jeans-do-women-own/</link>
		<description></description>
		<pubDate>Thu, 05 May 2011 11:57:12 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:57:12.767</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>Percent of consumers who love or enjoy wearing denim</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/004-Percent-consumers-wearing-denim/</link>
		<description>Over time, more than 7 out of 10 consumers say they love or enjoy wearing denim.</description>
		<pubDate>Thu, 05 May 2011 11:57:01 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:57:01.75</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>Percent of men &amp; women who love or enjoy wearing denim</title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/005-Percent-men-women-wearing-denim/</link>
		<description>More than 7 out of 10 men and women say they love or enjoy wearing denim.</description>
		<pubDate>Thu, 05 May 2011 11:56:50 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:56:50.627</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>When purchasing denim jeans, are consumers more concerned with looking good or being practical? </title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/006-Consumer-Denim-Jean-Purchases/</link>
		<description>When purchasing denim jeans, are consumers more concerned with looking good or being practical?  </description>
		<pubDate>Thu, 05 May 2011 11:56:39 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:56:39.36</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

	<item>
		<title>When purchasing denim jeans, are men and women more concerned with looking good or being practical? </title>
		<link>http://lifestylemonitor.cottoninc.com/LSM-Fast-Facts/007-Denim-Jean-Purchases-Men-and-Women/</link>
		<description>When purchasing denim jeans, women are more likely than men to say they are more concerned with looking good than being practical (58% versus 38%).   </description>
		<pubDate>Thu, 05 May 2011 11:56:26 EST</pubDate>
		<guid isPermaLink="false">2011-05-05 11:56:26.08</guid>
		<author>EThompson@cottoninc.com (Emily Thompson)</author>
	</item>

</channel>
</rss>
